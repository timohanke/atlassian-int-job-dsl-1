def plugins = [
   [
    product: 'CONFL',
    name: 'TNG-Profile', 
    gitUrl: 'gitolite@git.tngtech.com:tng-confluence-addon-profile-export',
    locks: [ 'confluence' ],
    description: 'TNG Profiles for Confluence.'
   ],
]

plugins.each { plugin ->

    job(type: Freeform) {
        name "ATLASSIAN-INT-${plugin.product}-${plugin.name}"
        description """\
${plugin.description}
<b>Attention:</b>
Changes to this generated job will be overridden during next generation!
"""
        logRotator(-1, 10, -1, -1)

        scm {
            git(plugin.gitUrl, 'origin/master') { node ->
                node / localBranch << 'master'
                node / skipTag << 'true'
            }
        }

        triggers {
            cron 'H 6 * * *'
            scm 'H/5 * * * *'
        }

        steps { 
            shell('libreoffice --headless --accept="socket,host=localhost,port=8100;urp;" --nofirststartwizard')
            maven('-B clean integration-test package', '') { node ->
                node / mavenName << 'Current Atlassian Plugin SDK'
            }
        }

        publishers {
            archiveArtifacts 'target/*.jar'
            if (!plugin.containsKey('noTests')) archiveJunit 'target/surefire-reports/*.xml'
/*
            // TODO there is a bug in 1.16, fixed in 1.17 
            // (see https://groups.google.com/forum/?fromgroups#!searchin/job-dsl-plugin/mailer/job-dsl-plugin/fxsTo-yX13k/faoNLX5BAxEJ)
            mailer('atlassian-development@tngtech.com', true, true) 
*/
        }

        configure { project -> 
            def buildWrappers = project / 'buildWrappers'

            buildWrappers << 'hudson.plugins.ws__cleanup.PreBuildCleanup' {
                deleteDirs false
                cleanupParameter ''
            }

            if (plugin.locks) {
                def locks = buildWrappers / 'hudson.plugins.locksandlatches.LockWrapper' / 'locks'
                plugin.locks.each { lockName ->
                    locks << 'hudson.plugins.locksandlatches.LockWrapper_-LockWaitConfig' {
                        // use "createNode" instead of "name 'confluence'" due to "name" is already reserved by Job DSL
                        createNode('name', lockName) 
                    }
                }
            }

            buildWrappers << 'hudson.plugins.release.ReleaseWrapper' {
                preBuildSteps {
                    'hudson.tasks.Maven' {
                        targets '-B versions:set -DnewVersion=\$RELEASE_VERSION'
                        mavenName 'Current Atlassian Plugin SDK'
                        usePrivateRepository false
                    }
                    'hudson.tasks.Shell' {
                        command """\
git commit pom.xml -m "releasing version \$RELEASE_VERSION"
git tag \$RELEASE_VERSION
"""
                    }
                }

                postSuccessfulBuildSteps {
                    'hudson.tasks.Maven' {
                        targets '-B -e package deploy:deploy --fail-never'
                        mavenName 'Current Atlassian Plugin SDK'
                        usePrivateRepository false
                    }
                    'hudson.tasks.Maven' {
                        targets '-B versions:set -DnewVersion=\$DEVELOPMENT_VERSION'
                        mavenName 'Current Atlassian Plugin SDK'
                        usePrivateRepository false
                    }
                    'hudson.tasks.Shell' {
                        command """\
git commit pom.xml -m "preparing for next iteration (\$DEVELOPMENT_VERSION)"
git push origin master
git push --tags origin master
"""
                    }
                }
            }
        }
    }
}